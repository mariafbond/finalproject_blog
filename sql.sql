SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";

SET time_zone = "+00:00";

CREATE DATABASE IF NOT EXISTS `blog` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;

USE `blog`;

CREATE TABLE category (
id int NOT NULL AUTO_INCREMENT,
cat varchar(255) NOT NULL,
PRIMARY KEY (id)
);

CREATE TABLE author (
id int NOT NULL AUTO_INCREMENT,
author varchar(255) NOT NULL,
authorbio text NULL,
PRIMARY KEY (id)
);

CREATE TABLE member (
id int NOT NULL AUTO_INCREMENT,
email varchar(255) NOT NULL,
name varchar(255) NOT NULL,
PRIMARY KEY (id)
);

CREATE TABLE comments (
id int NOT NULL AUTO_INCREMENT,
comment text NOT NULL,
nick varchar(255) NOT NULL,
PRIMARY KEY (id)
);


CREATE TABLE article (
id int NOT NULL AUTO_INCREMENT,
day int NULL,
date varchar(255) NULL,
fk_author int NOT NULL,
title varchar(500) NOT NULL,
cont text NULL,
summary varchar(500) NULL,
fk_cat int NOT NULL,

PRIMARY KEY (id),
FOREIGN KEY (fk_author) REFERENCES author(id),
FOREIGN KEY (fk_cat) REFERENCES category(id)
)engine =innodb;


INSERT INTO category (id, cat) VALUES

(1, 'B.Creative'),

(2, 'B.Tech'),

(3, 'B.Yourself');



INSERT INTO author (id, author, authorbio) VALUES

(1, 'Abisoye Ajayi-Akinfolarin', 'Abisoye (33) is a Social Impact Entrepreneur in Nigeria. She\'s the founder of GirlsCoding, an NGO that teaches girls how to code, design and build websites that help solve problems in their communities.'),

(2, 'Isabel Allende', 'Isabel (76), who was born in Peru to Chilean parents, is the world\'s most widely read Spanish-language author and has sold more than 70 million books in 42 languages.'),

(3, 'Sue Perkins', 'Sue (49) is a comedian, broadcaster, actress, and writer.'),

(4, 'Abbie Smith', 'Let\'s talk viruses. Not you and me, but I would like to introduce you to Abigail Smith. She is a virologist and studies endogenous retrovirus, and the reason I want you to know her is that she is an engaging speaker capable of explaining complex topics in simple terms. It\'s not necessary to have a good understanding of science or be interested in viruses to understand her and to be captivated by a topic that never seized your attention before. She has a few lectures on youtube, and I suspect that after you watch thes'),

(5, 'Athene Donald', 'I am a professor of physics in the Cavendish Laboratory at the University of Cambridge working on soft matter and biological physics. I have an active interest in issues around women in science within the university (WiSETI ) and outside (Athena Forum) and equality and diversity more generally; and a growing interest in education and science policy.  I hold a variety of positions on committees etc, but this blog is written in a purely personal capacity.\r\nMy first degree and my PhD are both in Physics from Cambridge University. After I spent 4 years at Cornell University in the USA I returned to Cambridge where I have been ever since. I became a professor in 1998 and was elected to the Royal Society in 1999 (I served on their Council 2004-6).  I was the 2009 Laureate for Europe of the L’Oreal/UNESCO For Women in Science Awards, and won the Faraday Medal of the Institute of Physics in 2010. I was appointed a DBE in the 2010 Birthday Honours.\r\nI am currently the University of Cambridge’s Gender Equality Champion, and from 2011 will chair the Royal Society’s Education Committee. I also chair Committee C of the BBSRC. I was the founding chair of the Institute of Physics Biological Physics Group (2006-10) and first Director of Cambridge University’s Physics of Medicine Initiative (2007-9).\r\nI am married to Matthew, a mathematician, and have two adult children.'),

(6, 'Laura Medalia', 'When she isn’t showing off her favorite new ironic tech t-shirt, Laura is documenting her experience as a software engineer in New York. Laura’s blog is evidence that technology can be creative, cross-disciplinary, and fun'),

(7, '@blondiebytes', 'My name is Kathryn Hodge\r\nI make programming tutorials \r\n'),

(8, 'Miriam Ballesteros', 'It\'s personal finance blogs that I read and money podcasts that I listen to almost every day that keep me so motivated and focused on my goals.\r\nAs a female personal finance blogger, I am passionate about sharing ideas, tools and resources that encourage women to make better money and career choices, and I love to see (and get inspired by) other women in the finance space who have a similar passion.'),

(9, 'Natalie Bacon', 'Hi Friend,\r\nI’m Natalie, and I want to share my story with you. \r\nNot because I\'m super special or have some amazing story or anything.\r\nQuite the opposite.\r\nI want to share my story with you because I get it.\r\nThe unfulfilling career, the uncertainty about what to do next in life, the desire for more time and money freedom.\r\nAll of it. I get it.\r\nI used to be an attorney. \r\nThen, I was a financial planner.\r\nI had massive (six figure) student loan debt.\r\nI wanted to work less and make more money.\r\nI wanted more fulfilling work.\r\nI knew something had to change. \r\nSo, I started a blog.\r\nI did this knowing nothing about online business or personal development.\r\nI always wanted to be a lawyer.\r\nBut everything changed because I took a chance and started my blog.\r\nIt led to me completely transforming my life with personal development, money, and business.'),

(10, 'Inna Yatsyna', 'Brand and Community Manager (English Region) at @Serpstat * a passionate lover of traveling, content marketing, and SEO * Let\'s be friends'),

(11, 'Sinem Gunel', ' 21 years old, digital entrepreneur, education and personal development enthusiast, '),

(12, 'Dora Dora', 'Founder and host of FemGems Podcast. Passionate about personal development, social impact and female leadership.'),

(13, 'CeCe Olisa', 'When I moved to New York City from California, I quickly noticed that as a plus size girl I was having a slightly different experience than my skinny friends. From dating, to finding wide calf boots, to job hunting things were different for me and I needed a place to sort through my own feelings of body insecurity and my constant quest to express myself with fashion. So, in 2008, as a hobby, I created this blog where I discuss fashion, fitness, dating and life from a plus size and body positive perspective.');







INSERT INTO article (id, day, date, fk_author, title, cont, summary, fk_cat) VALUES

(1, 1, 'March 2019', 1, 'Disadvantaged girls change their communities by learning to code', 'When I went to Makoko for the first time, I was surprised to see the living conditions of human beings. Most girls are trapped in a vicious cycle of poverty. Many of them are not thinking education, a plan for the future. But several times a week these girls get a glimpse of another world when they attend GirlsCoding, a free program run by the Pearls Africa Foundation that seeks to educate -and excite- girls about computer programming.', NULL, 2),

(2, 14, 'February 2019', 2, 'A \'happy\' life', 'I never said I wanted a \'happy\' life but an interesting one. From separation and loss, I have learned a lot. I have become strong and resilient, as is the case of almost every human being exposed to life and to the world. We don\'t even know how strong we are until we are forced to bring that hidden strength forward. In times of tragedy, of war, of necessity, people do amazing things. The human capacity for survival and renewal is awesome.\r\n', NULL, 1),

(3, 20, 'February 2019', 3, 'How I overcame a debilitating lifelong fear of travelling', 'I am deep within the forest, the raw heat of the day softened by the canopy above. The man in front of me is swaying, a gobbet of raw pig?s liver hanging from his ear. As I draw nearer, someone flicks a bit of kidney in my direction, which hits the white of my eye. Nearer still and the tribesmen rush to adorn my shoulders with offal. I think I can make out a spleen on my shirt collar, but am not sure. I was never any good at biology.', NULL, 3);



INSERT INTO member (id, email, name) VALUES
(1, 'abrown@mail.com', 'Alan Brown'),
(2, 'bbrown@mail.com', 'Brenda Brown'),
(3, 'cbrown@mail.com', 'Caroline Brown'),
(4, 'dbrown@mail.com', 'David Brown'),
(5, 'ebrown@mail.com', 'Emma Brown'),
(6, 'fbrown@mail.com', 'Frank Brown');


INSERT INTO comments (id, comment, nick)  VALUES
(1, 'love this', 'abrown');
