<?php
class AdminController {
    
public function articleCreate (){
     if($_SERVER['REQUEST_METHOD'] == 'POST'){
           
            Article::add();
             
            $articles = Article::all(); //$products is used within the view
            echo 'new article added ';
}
}    
    
public function articleReadAll() {
      // we store all the posts in a variable
      $articles = Article::all();
      
      require_once('views/admin/adminReadAll.php');
    } 
 
    public function articleDelete() {
            Article::remove($_GET['id']);
            
            $articles = Article::all();
            require_once('views/admin/adminReadAll.php');
      }
      
      function authorCreate(){
  if($_SERVER['REQUEST_METHOD'] == 'POST'){
    Author::add();
          
   $authors = Author::all();
   echo 'new author added';
}
}
      
      public function authorReadAll() {
      // we store all the posts in a variable
      $authors = Author::all();
      
      require_once('views/admin/authorAdminReadAll.php');
    } 
 
    public function authorDelete() {
            Author::remove($_GET['id']);
            
            $authors = Author::all();
            
            require_once('views/admin/authorAdminReadAll.php');
      }
   public function commentReadAll() {
      // we store all the posts in a variable
      $comments = Comment::all();
      
      require_once('views/admin/commentAdminReadAll.php');
    } 
 
    public function commentDelete() {
            Comment::remove($_GET['id']);
            
            $comments = Comment::all();
            
            require_once('views/admin/commentAdminReadAll.php');
      }
      
    public function commentUpdate() {
        
      if($_SERVER['REQUEST_METHOD'] == 'GET'){
          if (!isset($_GET['id']))
        return call('home', 'error');

        // we use the given id to get the correct product
        $comment = Comment::find($_GET['id']);
        
        require_once('views/admin/commentUpdate.php');
        }
      else
          { 
            $id = $_GET['id'];
            Comment::update($id);
                        
            $comments = Comment::all();
            require_once('views/admin/commentAdminReadAll.php');
      }
      
    } 
    
    public function home() {
        require 'views/pages/adminhome.php';
    }

}
