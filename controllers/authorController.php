<?php
class AuthorController {
    public function readAll() {
      // we store all the posts in a variable
      $authors = Author::all();
      
      require_once('views/author/authorReadAll.php');
    }

    public function read() {
      // we expect a url of form ?controller=posts&action=show&id=x      
// without an .id we just redirect to the error page as we need the post id to find it in the database
       
      if (!isset($_GET['id']))
          return call('home', 'error');

      try {
      // we use the given id to get the correct post
         
      $author = Author::find($_GET['id']);
      require('views/author/authorRead.php');
      }
catch (Exception $ex){
     
    return call('home','');
    }}
    
   
    public function create() {
      // we expect a url of form ?controller=products&action=create
      // if it's a GET request display a blank form for creating a new product
      // else it's a POST so add to the database and redirect to readAll action
      if($_SERVER['REQUEST_METHOD'] == 'GET'){
          require('views/author/authorCreate.php');
      }
      else { 
          
          Author::add();
          
          
             
            $authors = Author::all(); //$authors is used within the view
            require('views/author/authorReadAll.php');
      }
      
    }
    public function update() {
        
      if($_SERVER['REQUEST_METHOD'] == 'GET'){
          if (!isset($_GET['id']))
        return call('home', 'error');

        // we use the given id to get the correct product
        $author = Author::find($_GET['id']);
        
        require_once('views/author/authorUpdate.php');
        }
      else
          { 
            $id = $_GET['id'];
            Author::update($id);
                        
            $authors = Author::all();
            require_once('views/author/authorReadAll.php');
      }
      
    }
    public function delete() {
            Member::remove($_GET['id']);
            
            $author = Author::all();
            require_once('views/author/authorReadAll.php');
      }
      
    }
?>