<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
             <title>33 Million Stories </title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link href="views/css/BootstrapCSS.css" rel="stylesheet" type="text/css"/>
    <link href="https://fonts.googleapis.com/css?family=Karla|Special+Elite" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Nanum+Gothic+Coding" rel="stylesheet">
    <link rel="icon" href="views/images/authorimages/women_empowered.jpg">
    <script src="jquery/jquery-3.3.1.js" type="text/javascript"></script>
    </head>
    <body>
           
        <?php
    require_once('connection.php');
     
    if (isset($_GET['controller']) && isset($_GET['action'])) {
        $controller = $_GET['controller'];
        $action     = $_GET['action'];
      
  } else {
        $controller = 'home';
        $action     = 'home';
        
 
  }
  
 
        ?>

       
  <div class="container">
   
    
      <div class="nav-scroller pb-1 mb-2">
        <nav class="nav d-flex justify-content-between">
          <a class="p-2 text-muted" href="/FinalProject_Blog/index.php">Home</a>
          <a class="p-2 text-muted" id="b-tech" href="?controller=article&action=theme&id=2">B.Tech</a>
          <a class="p-2 text-muted" id="b-you" href="?controller=article&action=theme&id=3">B.Yourself</a>
          <a class="p-2 text-muted" id="b-create" href="?controller=article&action=theme&id=1">B.Creative</a>
          <a class="p-2 text-muted" href="views/pages/Login.php">Log in</a>

        
           <!-- SEARCH  SCRIPT. ????? SHOULD THIS GO IN A SEPARATE JQERY FILE??--> 
           
<script src="jquery/jquery-3.3.1.js" ></script>
<script >

function do_search()
{
 var search_term=$("#search_term").val();
 $.ajax
 ({
  type:'post',
  url:'search/search.php',
  data:{
   search:"search",
   search_term:search_term
  },
  success:function(response) 
  {
   document.getElementById("result_div").innerHTML=response;
  }
 });
 
 return false;
}
</script> 

<div id="search_box">
            <form class="form-inline mt-2 mt-md-0" method="post" onsubmit="return do_search();">
            <input class="form-control mr-sm-2" type="text" id="search_term" name="search_term" placeholder="Enter Search" aria-label="Search" ;">
            <button class="btn btn-outline-info my-2 my-sm-0t" type="submit"name="search" value="SEARCH">Search</button>
            </form>
            
          </div> 

        </nav>
          
      </div>


<!-- Main panel with blog name and background image-->

            <!-- Carousel -->
            <!-- Surround everything with a div with the class carousel slide -->
            <div id="theCarousel" class="carousel slide" data-ride="carousel">

<!--                 Define how many slides to put in the carousel -->
                <ol class="carousel-indicators">
                    <li data-target="#theCarousel" data-slide-to="0" class="active"> </li >
                    <li data-target="#theCarousel" data-slide-to="1"> </li>
                    <li data-target ="#theCarousel" data-slide-to="3"> </li>
                    <li data-target ="#theCarousel" data-slide-to="4"> </li>
                </ol >

                <a class="left carousel-control" href="#theCarousel" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left"> </span>
                </a>
                <a class="right carousel-control" href="#theCarousel" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right"></span>
                </a>
            </div>

           

            <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
                <div class="carousel-inner">
                    <div class="carousel-item active">
                        <img class="d-block w-100" src="views/images/themes/Carousel_4_stories.png" alt="First slide">
                        <div class="carousel-caption d-none d-xs-block">
                            
                        </div>
                    </div>
                    <div class="carousel-item">
                        <img class="d-block w-100" src="views/images/themes/Carousel_2_tech.png" alt="Second slide">
                    </div>
                    <div class="carousel-item">
                        <img class="d-block w-100" src="views/images/themes/Carousel_3_yourself.png" alt="Third slide">
                    </div>
                    <div class="carousel-item">
                        <img class="d-block w-100" src="views/images/themes/Carousel_1_creative.png" alt="Third slide">
                    </div>
                </div>
                <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a> 
            </div>
        </div>  
        <br><br>
        <div class="container">
            
<div id='result_div'>
<div id='coverup'>
    
<?php require_once 'routes.php';?>
</div>  

</div>
<!-- Links to related articles-->      
     
        <!-- FOOTER with links to Privacy Policy and Contact Form -->
        </div>
        <div class="container ">
            <footer class="p-4" >
        <p class="float-right"><a href="#">Back to top</a></p>
        <p>&copy; Codettes 2019 &middot; <a href="views/pages/priv.php">Privacy Policy</a>&middot; <a href="views/pages/newForm.php">Contact Us</a>&middot;<a href="?controller=admin&action=home">admin</a></p>
    </footer>
   

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster--> 
 <!-- this is another search stopping issue <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>-->
    <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery-slim.min.js"><\/script>');</script>
    <script src="../../assets/js/vendor/popper.min.js"></script>
    <script src="../../dist/js/bootstrap.min.js"></script>
    <script src="../../assets/js/vendor/holder.min.js"></script>
    <script>
      Holder.addTheme('thumb', {
        bg: '#55595c',
        fg: '#eceeef',
        text: 'Thumbnail'
      });
    </script>
  <!--- this stops search function working  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>-->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
   <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
   <!-- Tinymce script stuff -->
   <script src="https://cloud.tinymce.com/5/tinymce.min.js"></script>
        <script>tinymce.init({ selector:'textarea.wysiwyg', height: '450px'});</script>
  </div>
  </body>
</html>
