<?php

  require_once('dbconn.php');
$article_id  = trim($_POST["article_id"]);
  $message  = trim($_POST["message"]);
  $date    = trim($_POST["date"]);
  $uid  = trim($_POST["uid"]);
  $cid  = trim($_POST["cid"]);
// prepare sql and bind parameters
    $stmt = $dbconn->prepare("UPDATE comments SET uid = :uid, date = :date, message = :message, article_id = :article_id WHERE cid = :cid");
    $stmt->bindParam(':article_id', $article_id);
    $stmt->bindParam(':message', $message);
    $stmt->bindParam(':date', $date);
    $stmt->bindParam(':uid', $uid);
    $stmt->bindParam(':cid', $cid);
    // insert a row
    if($stmt->execute()){
      $result =1;
    }
    echo $result;
    $dbconn = null;