<?php
  require_once('dbconn.php');

   $cid  = trim($_GET["cid"]);

  // prepare sql and bind parameters
    $stmt = $dbconn->prepare("select * from comments where cid = :cid");
    $stmt->bindParam(':cid', $cid);
    // insert a row
    $stmt->execute();
    $data = $stmt->fetch(PDO::FETCH_ASSOC);
    echo json_encode($data);
    $dbconn = null;

