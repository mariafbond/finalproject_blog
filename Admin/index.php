<?php

require_once('dbconn.php');
$sth = $dbconn->prepare("SELECT * FROM comments order by cid desc");
$sth->execute();
/* Fetch all of rows in the result set */
$result = $sth->fetchAll();

?>
<!DOCTYPE html>
<html>
<head>
<style>
.container{
  margin: 20px auto;
}
h2 {
  text-align: center;
}
table {
    font-family: arial, sans-serif;
    border-collapse: collapse;
    width: 100%;
}

td, th {
    border: 1px solid #dddddd;
    text-align: left;
    padding: 8px;
}

tr:nth-child(even) {
    background-color: #dddddd;
}

body{
  font-family:Arial, Helvetica, sans-serif;
  font-size:13px;
}
.success, .error{
  border: 1px solid;
  margin: 10px 0px;
  padding:15px 10px 15px 50px;
  background-repeat: no-repeat;
  background-position: 10px center;
}

.success {
  color: #4F8A10;
  background-color: #DFF2BF;
  background-image:url('success.png');
  display: none;
}
.error {
  display: none;
  color: #D8000C;
  background-color: #FFBABA;
  background-image: url('error.png');
}
</style>
</head>
<body>
  <div class="container">
    <h2>Remove inappropriate/ offensive comments</h2>
    <div class="success"></div>
    <div class="error"></div>
    <form>
       <table>
        <tr>
            <td colspan="4" rowspan="3" style="text-align: center">
            <input type="hidden" id ='cid' value='' />
                 <input type='text' id='uid' placeholder='User' required />&nbsp;&nbsp;
                 <input type='datetime' id='date' placeholder='Date' required />&nbsp;&nbsp;
            <input type='text' id='message' placeholder='Message' required />&nbsp;&nbsp;
              <input type='text' id='article_id' placeholder='Article ID' required />&nbsp;&nbsp;
          <input type='button' id='saverecords'  value ='Change Record' /></td>
        </tr>
      </table>
    </form>
    <h2>View Records</h2>
    <table>
      <tr>
        <th>#</th>
        <th>User</th>
        <th>Date</th>
        <th>Message</th>
        <th>Article ID</th>
        <th>Action</th>
      </tr>
  <?php
  /* FetchAll foreach with edit and delete using Ajax */
  if($sth->rowCount()):
   foreach($result as $row){ ?>
     <tr>
       <td><?php echo $row['cid']; ?></td>
       <td><?php echo $row['uid']; ?></td>
       <td><?php echo $row['date']; ?></td>
       <td><?php echo $row['message']; ?></td>
        <td><?php echo $row['article_id']; ?></td>
      
       <td><a data-pid = '<?php echo $row['cid']; ?>' class='editbtn' href= 'javascript:void(0)'>Edit Record</a>&nbsp;|&nbsp;
           <a data-pid=<?php echo $row['cid']; ?> class= 'delbtn' href= 'javascript:void(0)'>Delete</a></td>
     </tr>
   <?php }  ?>
  <?php endif;  ?>
  </table>
  </div>
  <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
  <script>
    $(function(){

      /* Delete button ajax call */
      $('.delbtn').on( 'click', function(){
        if(confirm('This action will delete this record. Are you sure?')){
          var pid = $(this).data('pid');
          $.post( "delete_record_ajax.php", { cid: pid })
          .done(function( data ) {
            if(data > 0){
              $('.success').show(3000).html("Record deleted successfully.").delay(3200).fadeOut(6000);
            }else{
              $('.error').show(3000).html("Record could not be deleted. Please try again.").delay(3200).fadeOut(6000);;
            }
            setTimeout(function(){
                window.location.reload(1);
            }, 5000);
          });
        }
      });

     /* Edit button ajax call */
      $('.editbtn').on( 'click', function(){
          var pid = $(this).data('pid');
          $.get( "getrecord_ajax.php", { cid: pid })
            .done(function( uid ) {
              data = $.parseJSON(uid);

              if(data){
                $('#cid').val(data.cid);
                $('#uid').val(data.uid);
                $('#date').val(data.date);
                $('#message').val(data.message);
                $('#article_id').val(data.article_id);
                $("#saverecords").val('Save Records');
            }
          });
      });

      /* Edit button ajax call */
       $('#saverecords').on( 'click', function(){
           var cid  = $('#cid').val();
           var uid = $('#uid').val();
           var date   = $('#date').val();
           var message = $('#message').val();
           var article_id   = $('#article_id').val();
           if(!uid || !date || !message || !article_id ){
             $('.error').show(3000).html("All fields are required.").delay(3200).fadeOut(3000);
           }else{
                if(cid){
                var url = 'edit_record_ajax.php';
              }else{
                var url = 'add_records_ajax.php';
              }
                $.post( url, {cid:cid, uid: uid, date: date, message: message, article_id: article_id })
               .done(function( data ) {
                 if(data > 0){
                   $('.success').show(3000).html("Record saved successfully.").delay(2000).fadeOut(1000);
                 }else{
                   $('.error').show(3000).html("Record could not be saved. Please try again.").delay(2000).fadeOut(1000);
                 }
                 $("#saverecords").val('Add Records');
                 setTimeout(function(){
                     window.location.reload(1);
                 }, 15000);
             });
          }
       });
    });
 </script>
</body>
</html>