
<?php

require_once('dbconn.php');

$sth = $dbconn->prepare("SELECT article.article_id, article.date, author.author, article.title, article.cont FROM article INNER JOIN author on article.fk_author = author.author_id order by article_id desc");
$sth->execute();
/* Fetch all of rows in the result set */
$result = $sth->fetchAll();

?>
<!DOCTYPE html>
<html>
<head>
<style>
.container{
    height: 10em;
  margin: 60px auto;
  position: relative;
}
h2 {
  text-align: center;
}
table {
    font-family: arial, sans-serif;
    border-collapse: collapse;
    width: 100%;
}

td, th {
    border: 1px solid #dddddd;
    text-align: left;
    padding: 8px;
}

tr:nth-child(even) {
    background-color: #dddddd;
}

body{
  font-family:Arial, Helvetica, sans-serif;
  font-size:13px;
}
.success, .error{
  border: 1px solid;
  margin: 10px 0px;
  padding:15px 10px 15px 50px;
  background-repeat: no-repeat;
  background-position: 10px center;
}

.success {
  color: #4F8A10;
  background-color: #DFF2BF;
  background-image:url('success.png');
  display: none;
}
.error {
  display: none;
  color: #D8000C;
  background-color: #FFBABA;
  background-image: url('error.png');
}
</style>
</head>
<body>
  <div class="container">
    <h2>View articles in the database</h2>
    <!--<div class="success"></div>
    <div class="error"></div>
    <h2></h2>
    <form>
       <table>
        <tr>
          <td colspan="4" style="text-align: center">
          <input type="hidden" id ='article_id' value='' />
          <input type='text' id='date' placeholder='Date' required />&nbsp;&nbsp;
          <input type='text' id='fk_author' placeholder='Author' required />&nbsp;&nbsp;
          <input type='text' id='title' placeholder='Title' required />&nbsp;&nbsp;
          <input type='text' id='cont' placeholder='Article' required />&nbsp;&nbsp;
          <input type='button' id='saverecords'  value ='Add Records' /></td>
        </tr>
      </table>
    </form>-->
    <h2></h2>
    <table>
      <tr>
        <th>#</th>
        <th>Date</th>
        <th>Author</th>
        <th>Title</th>
        <th>Article</th>
      </tr>
      
      <?php
  /* FetchAll foreach with edit and delete using Ajax */
  if($sth->rowCount()):
   foreach($result as $row){ ?>
     <tr>
       <td><?php echo $row['article_id']; ?></td>
       <td><?php echo $row['date']; ?></td>
       <td><?php echo $row['author']; ?></td>
       <td><?php echo $row['title']; ?></td>
       <td><?php echo strip_tags(html_entity_decode(substr($row['cont'],0,350))); ?></td>
       <!--<td><a data-pid = <?php echo $row['article_id']; ?> class='editbtn' href= 'javascript:void(0)'>Edit</a>&nbsp;|&nbsp;-->
          
     </tr>
   <?php }  ?>
  <?php endif;  ?>
  </table>
  </div>
  <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
  <script>
    $(function(){

      

     /* Edit button ajax call */
      $('.editbtn').on( 'click', function(){
          var pid = $(this).data('pid');
          $.get( "getrecord_ajax.php", { aprticle_id: pid })
            .done(function( day ) {
              data = $.parseJSON(day);

              if(data){
                $('#article_id').val(data.article_id);
                $('#date').val(data.date);
                $('#fk_author').val(data.fk_author);
                $('#title').val(data.title);
                $('#cont').val(data.cont);
                $("#saverecords").val('Save Records');
            }
          });
      });

      /* Edit button ajax call */
       $('#saverecords').on( 'click', function(){
           var id  = $('#article_id').val();
           var date   = $('#date').val();
           var fk_author = $('#fk_author').val();
           var title  = $('#title').val();
           var cont = $('#cont').val();
           if(!date || !fk_author || !title || !cont){
             $('.error').show(3000).html("All fields are required.").delay(3200).fadeOut(3000);
           }else{
                if(id){
                var url = 'edit_record_ajax.php';
              }else{
                var url = 'add_records_ajax.php';
              }
                $.post( url, {article_id: article_id, date: date, fk_author: fk_author, title: title, cont: cont, summary: summary, fk_cat: fk_cat})
               .done(function( data ) {
                 if(data > 0){
                   $('.success').show(3000).html("Record saved successfully.").delay(2000).fadeOut(1000);
                 }else{
                   $('.error').show(3000).html("Record could not be saved. Please try again.").delay(2000).fadeOut(1000);
                 }
                 $("#saverecords").val('Add Records');
                 setTimeout(function(){
                     window.location.reload(1);
                 }, 15000);
             });
          }
       });
    });
 </script>
</body>
</html>