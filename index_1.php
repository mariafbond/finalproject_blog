<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
             <title>33 Million Stories </title>
 <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<link href="views/css/BootstrapCSS.css" rel="stylesheet" type="text/css"/>
    <link rel="icon" href=views/images/women_empowered.jpg">
<script src="jquery/jquery-3.3.1.js" type="text/javascript"></script>
    
    </head>
    <body>
        <?php
    require_once('connection.php');
     
    if (isset($_GET['controller']) && isset($_GET['action'])) {
        $controller = $_GET['controller'];
        $action     = $_GET['action'];
      
  } else {
        $controller = 'home';
        $action     = 'home';
        
 
  }
  
 
        ?>
       
  <div class="container">
     
        <!-- Leaving the header to add search box-->
        <header class="blog-header py-3"></header>
     
     
<!-- Links to subpages -->


        
    
      <div class="nav-scroller py-1 mb-2">
        <nav class="nav d-flex justify-content-between">
          <a class="p-2 text-muted" href="http://localhost:8080/FinalProject_Blog/index.php">Home</a>
          <a class="p-2 text-muted" id="b-tech" href="#">B.Tech</a>
          <a class="p-2 text-muted" id="b-you" href="#">B.Yourself</a>
          <a class="p-2 text-muted" id="b-create" href="#">B.Creative</a>
          <a class="p-2 text-muted" href="views/pages/Login.php">Log in</a>

        
           <!-- SEARCH  SCRIPT. ????? SHOULD THIS GO IN A SEPARATE JQERY FILE??--> 
           
<script src="jquery/jquery-3.3.1.js" ></script>
<script >

function do_search()
{
 var search_term=$("#search_term").val();
 $.ajax
 ({
  type:'post',
  url:'search/search.php',
  data:{
   search:"search",
   search_term:search_term
  },
  success:function(response) 
  {
   document.getElementById("result_div").innerHTML=response;
  }
 });
 
 return false;
}
</script> 

<div id="search_box">
            <form class="form-inline mt-2 mt-md-0" method="post" onsubmit="return do_search();">
            <input class="form-control mr-sm-2" type="text" id="search_term" name="search_term" placeholder="Enter Search" aria-label="Search" ;">
            <button class="bn btn-outline-success my-2 my-sm-0t" type="submit"name="search" value="SEARCH">Search</button>
            </form>
            
          </div> 

        </nav>
          
      </div>


<!-- Main panel with blog name and background image-->

         <div class="container" id="spark">  
          <div class="jumbotron" id='themey' style="background-image: url('views/images/themes/spark.png')" alt="">
            <div class="text-center">     
                <h1 class="display-3" class="center"></h1>
                <h1 class="display-5" id="blog-title">33 million stories</h1>
                <h2 class="lead my-5" id="blog-subtitle"> 33 million million ways to inspire</h2>
                    <!--<p class="lead mb-0"><a href="nbproject/aboutUs.php" class="text-white font-weight-bold">Continue reading...</a></p>-->       
            </div>        
        </div>
      </div>

  
<div id='result_div'>
<div id='coverup'>
<?php require_once 'routes.php';?>
</div>
</div>    

<!-- jquery files-->     
<script>
            
               
               $("#b-tech").click(function(){
               /*$('.jumbotron').fadeOut(100);*/    
               $('.jumbotron').css('background-image','url(views/images/themes/circutsbanner.png)');
               $('.jumbotron').fadeIn(600);
               $('#blog-title').css("font-size", "72px");
               $('#blog-title').css("text-shadow", "2px 2px 5px black");
               $('#blog-title').html("B.Tech");
               $('#blog-title').css("color","#FEC509");
               $('#blog-subtitle').css("text-shadow", "2px 2px 5px black");
               $('#blog-subtitle').html("<strong>33 Million Stories</strong>");
               $('#blog-subtitle').css("color","#FEC509");
                 });
                 
               $("#b-you").click(function(){
               /*$('.jumbotron').fadeOut(100);*/    
               $('.jumbotron').css('background-image','url(views/images/themes/yogajumbo.jpg)');
               $('.jumbotron').fadeIn(600);
               $('#blog-title').html("B.Yourself");
               $('#blog-title').css("font-size", "72px");
               $('#blog-title').css("color","#14CBE8");
               $('#blog-title').css("text-shadow", "2px 2px 5px black");
               $('#blog-subtitle').html("<strong>33 Million Stories</strong>");
               $('#blog-subtitle').css("text-shadow", "2px 2px 5px black");
               $('#blog-subtitle').css("color","#14CBE8");
                 });
                 
               $("#b-create").click(function(){
               /*$('.jumbotron').fadeOut(100);*/    
               $('.jumbotron').css('background-image','url(views/images/themes/creative.2.jpg)');
               $('.jumbotron').fadeIn(600);
               $('#blog-title').html("B.Creative");
               $('#blog-title').css("text-shadow", "2px 2px 5px #FEFEBE");              
               $('#blog-title').css("color","#312F23");
               $('#blog-subtitle').html("<strong>33 Million Stories</strong>");
               $('#blog-subtitle').css("text-shadow", "2px 2px 5px #FEFEBE"); 
              
               $('#blog-subtitle').css("color","#312F23");
                 });
                 
            
        </script>  
     
        <!-- FOOTER with links to Privacy Policy and Contact Form -->
    
    <footer class="container">
        <p class="float-right"><a href="#">Back to top</a></p>
        <p>&copy; Codettes 2019 &middot; <a href="nbproject/priv.php">Privacy Policy</a>&middot; <a href="nbproject/contactUs.php">Contact Us</a></p>
    </footer>
        
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster--> 
 <!-- this is another search stopping issue <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>-->
    <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery-slim.min.js"><\/script>');</script>
    <script src="../../assets/js/vendor/popper.min.js"></script>
    <script src="../../dist/js/bootstrap.min.js"></script>
    <script src="../../assets/js/vendor/holder.min.js"></script>
    <script>
      Holder.addTheme('thumb', {
        bg: '#55595c',
        fg: '#eceeef',
        text: 'Thumbnail'
      });
    </script>
  <!--- this stops search function working  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>-->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
   <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
   <!-- Tinymce script stuff -->
   <script src="https://cloud.tinymce.com/5/tinymce.min.js"></script>
        <script>tinymce.init({ selector:'#wysiwyg' });</script>
  </body>
</html>
