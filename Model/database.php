<?php


class Article  {
    
    //we difine attributes
    public $id;
    public $day;
    public $date;
    public $fkauthor;
    public $title;
    public $cont;
    public $summary;
    public $fkcat;
    public $author;
    
    
    public function __construct($id, $day, $date, $fk_author, $title, $cont, $summary, $fk_cat, $author){
        $this->id   = $id;
        $this->day  = $day;
        $this->date = $date;
        $this->fk_author = $fk_author;
        $this->title = $title;
        $this->cont  = $cont;
        $this->summary = $summary;
        $this->fk_cat = $fk_cat;
        $this->author = $author;
        
       
    }
    
    public static function all(){
        $list=[];
        $db =Db::getinstance(); //we need to make sure that the database object is called this
        $req = $db->query('SELECT article.article_id, article.day, article.date, article.fk_author, article.title, article.cont, article.summary, article.fk_cat, author.author FROM article INNER JOIN author ON article.fk_author = author.author_id ORDER BY date DESC');

        //create a list of the article objects from the database results
        foreach($req->fetchAll() as $article)            
            {         
        $list[] = new Article($article['article_id'], $article['day'], $article['date'], $article['fk_author'], $article['title'], $article['cont'], $article['summary'], $article['fk_cat'], $article['author']/* $article['category']*/);
          }
    return $list;
    
        }
  
        
    public static function find($id){
        
        
        
        
        $db= Db::getInstance();
        $id=intval($id);
        $req = $db->prepare('SELECT article.article_id, article.day, article.date, article.fk_author, article.title, article.cont, article.summary, article.fk_cat, author.author, category.cat FROM ((article INNER JOIN author ON article.fk_author = author.author_id) INNER JOIN category ON article.fk_cat = category.cat_id) WHERE article.article_id = :id');
      
        $req->execute(array('id'=> $id));
        $article = $req->fetch();
        if($article){
         return new Article($article['article_id'], $article['day'], $article['date'], $article['fk_author'], $article['title'], $article['cont'], $article['summary'], $article['fk_cat'], $article['author']);
          }
          else{
             throw new Exception('find Oh dear something didn\'t work. le sigh.');
     }
    }
     
     public static function feature($id){
        
        
        
        
         
        $db= Db::getInstance();
        $id=intval($id);
        /*something*/$req = $db->prepare('SELECT  article.article_id, article.day, article.date, article.fk_author, article.title, article.cont, article.summary, article.fk_cat, author.author, category.cat FROM (((article INNER JOIN author ON article.fk_author = author.author_id) INNER JOIN category ON article.fk_cat = category.cat_id) INNER JOIN featured ON featured.article_id = article.article_id) WHERE featured.id= :id');
      
        $req->execute(array('id'=> $id));
        $article = $req->fetch();
        if($article){
         return new Article($article['article_id'], $article['day'], $article['date'], $article['fk_author'], $article['title'], $article['cont'], $article['summary'], $article['fk_cat'], $article['author']);
          }
          else{
             throw new Exception('Oh dear something didn\'t work. le sigh.');
     }
    }
    
    public static function theme($id){
        
        
        
        
        $db= Db::getInstance();
        $id=intval($id);
        $req = $db->prepare('SELECT article.article_id, article.day, article.date, article.fk_author, article.title, article.cont, article.summary, article.fk_cat, author.author, category.cat FROM ((article INNER JOIN author ON article.fk_author = author.author_id) INNER JOIN category ON article.fk_cat = category.cat_id) WHERE article.fk_cat = :id');
      
        $req->execute(array('id'=> $id));
        foreach($req->fetchAll() as $tarticle)
        { 
         $list[] = new Article($tarticle['article_id'], $tarticle['day'], $tarticle['date'], $tarticle['fk_author'], $tarticle['title'], $tarticle['cont'], $tarticle['summary'], $tarticle['fk_cat'], $tarticle['author']);
          }
          
         return $list;
     }
    
    
    public static function update($id){
        $db= Db::getInstance();
        $req = $db->prepare("Update article set day=:day, date=:date, fk_author= :fk_author, title= :title, cont= :cont, summary= :summary, fk_cat = :fk_cat WHERE article_id= :id");
        $req->bindParam('id', $id);
        $req->bindParam('day', $day);
        $req->bindParam('date', $date);
        $req-> bindParam('fk_author', $fk_author);
        $req-> bindParam('title', $title);
        $req-> bindParam('cont', $cont);
        $req-> bindParam('summary', $summary);
        $req-> bindParam('fk_cat',$fk_cat);
        
    // set name and price parameters and execute
       if(isset($_POST['day']) && $_POST['day'] !=""){
           $filteredDay = filter_input(INPUT_POST, 'day', FILTER_SANITIZE_SPECIAL_CHARS);
       }
       
       if(isset($_POST['date']) && $_POST['date'] !=""){
           $filteredDate = filter_input(INPUT_POST, 'date', FILTER_SANITIZE_SPECIAL_CHARS);
           
       }
       
       if(isset($_POST['fk_author']) && $_POST['fk_author'] !=""){
           $filteredFkAuthor = filter_input(INPUT_POST, 'fk_author', FILTER_SANITIZE_SPECIAL_CHARS);
       }
       
       if(isset($_POST['title']) && $_POST['title'] !=""){
           $filteredTitle = filter_input(INPUT_POST, 'title', FILTER_SANITIZE_SPECIAL_CHARS);
       }
       
       if(isset($_POST['cont']) && $_POST['cont'] !=""){
           $filteredCont = filter_input(INPUT_POST, 'cont', FILTER_SANITIZE_SPECIAL_CHARS);
       }
       
       if(isset($_POST['summary']) && $_POST['summary'] !=""){
           $filteredSummary = filter_input(INPUT_POST, 'summary', FILTER_SANITIZE_SPECIAL_CHARS);
       }
       
       if(isset($_POST['fk_cat']) && $_POST['fk_cat'] !=""){
           $filteredFkCat = filter_input(INPUT_POST, 'fk_cat', FILTER_SANITIZE_SPECIAL_CHARS);
       }
       
     $day = $filteredDay;
     $date = $filteredDate;
     $fk_author = $filteredFkAuthor;
     $title = $filteredTitle;
     $cont = $filteredCont;
     $summary = $filteredSummary;
     $fk_cat  = $filteredFkCat;
     $req-> execute();
     
     
     //upload product image if it exists
     
     if (!empty($_FILES[self::InputKey]['name'])) {
       $filename=$title.$fk_author;  
       Article:: uploadFile($filename);  
     }
       
       
    }
    
  public static function add()  {
    $db= Db::getInstance();
        $req = $db->prepare("Insert into article(day, date, fk_author, title, cont, summary, fk_cat) values (:day , :date, :fk_author, :title, :cont, :summary, :fk_cat)");
      //  $req->bindParam(':id', $id);
        $req->bindParam('day', $day);
        $req->bindParam('date', $date);
        $req-> bindParam('fk_author', $fk_author);
        $req-> bindParam('title', $title);
        $req-> bindParam('cont', $cont);
        $req-> bindParam('summary', $summary);
        $req-> bindParam('fk_cat',$fk_cat);
        
    // set name and price parameters and execute
       if(isset($_POST['day']) && $_POST['day'] !=""){
           $filteredDay = filter_input(INPUT_POST, 'day', FILTER_SANITIZE_SPECIAL_CHARS);
       }
       
       if(isset($_POST['date']) && $_POST['date'] !=""){
           $filteredDate = filter_input(INPUT_POST, 'date', FILTER_SANITIZE_SPECIAL_CHARS);
           
       }
       
       if(isset($_POST['fk_author']) && $_POST['fk_author'] !=""){
           $filteredFkAuthor = filter_input(INPUT_POST, 'fk_author', FILTER_SANITIZE_SPECIAL_CHARS);
       }
       
       if(isset($_POST['title']) && $_POST['title'] !=""){
           $filteredTitle = filter_input(INPUT_POST, 'title', FILTER_SANITIZE_SPECIAL_CHARS);
       }
       
       if(isset($_POST['cont']) && $_POST['cont'] !=""){
           $filteredCont = filter_input(INPUT_POST, 'cont', FILTER_SANITIZE_SPECIAL_CHARS);
       }
       
       if(isset($_POST['summary']) && $_POST['summary'] !=""){
           $filteredSummary = filter_input(INPUT_POST, 'summary', FILTER_SANITIZE_SPECIAL_CHARS);
       }
       
       if(isset($_POST['fk_cat']) && $_POST['fk_cat'] !=""){
           $filteredFkCat = filter_input(INPUT_POST, 'fk_cat', FILTER_SANITIZE_SPECIAL_CHARS);
       }
       
     $day = $filteredDay;
     $date = $filteredDate;
     $fk_author = $filteredFkAuthor;
     $title = $filteredTitle;
     $cont = $filteredCont;
     $summary = $filteredSummary;
     $fk_cat  = $filteredFkCat;
     $req-> execute();
   //upload article image
     $filename=$title.$fk_author;
     Article::uploadFile($filename);
       
  }
  
 const AllowedTypes = ['image/jpeg', 'image/jpg', 'image/png'];
const InputKey = 'myUploader';

//die() function calls replaced with trigger_error() calls//
//replace with structured exception handling
public static function uploadFile($article) {

	if (empty($_FILES[self::InputKey])) {
		//die("File Missing!");
                trigger_error("File Missing!");
	}

	if ($_FILES[self::InputKey]['error'] > 0) {
		trigger_error("Handle the error! " . $_FILES[InputKey]['error']);
	}


	if (!in_array($_FILES[self::InputKey]['type'], self::AllowedTypes)) {
		trigger_error("Handle File Type Not Allowed: " . $_FILES[self::InputKey]['type']);
	}

        
	$tempFile = $_FILES[self::InputKey]['tmp_name'];
        $filepath = "views/images/article/";
	$destinationFile = $filepath .str_replace(' ','',$article).'.jpg';
       // $path = "C:/xampp/htdocs/finalproject_blog/views/images/article/"; this is the pc path

	if (!move_uploaded_file($tempFile, $destinationFile)) {
		trigger_error("Handle Error");
	}
		
	//Clean up the temp file
	if (file_exists($tempFile)) {
		unlink($tempFile); 
	}
}

        


public static function remove($id) {
      $db = Db::getInstance();
      //make sure $id is an integer
      $id = intval($id);
      $req = $db->prepare('delete FROM article WHERE article_id = :id');
      // the query was prepared, now replace :id with the actual $id value
      $req->execute(array('id' => $id));
  }

}

  class Author {
// OKAY THIS IS THE BIT THAT WORKS WITH ALL THE SQL. SO YOU GOT YOUR CREATE ADD DELETE READ ETC CLASSES
// 
    // we define 3 attributes
    public $id;
    public $author;
    public $authorbio;

    public function __construct($id, $author, $authorbio) {
      $this->id    = $id;
      $this->author  = $author;
      $this->authorbio = $authorbio;
    }

    public static function all() {
      $list = [];
      $db = Db::getInstance();
      $req = $db->query('SELECT * FROM author');
      // we create a list of Product objects from the database results
      foreach($req->fetchAll() as $author) {
        $list[] = new Author($author['author_id'], $author['author'], $author['authorbio']);
      }
      return $list;
    }

    public static function find($id) {
        
      $db = Db::getInstance();
      //use intval to make sure $id is an integer
      $id = intval($id);
      $req = $db->prepare('SELECT * FROM author WHERE author_id = :id');
      //the query was prepared, now replace :id with the actual $id value
      $req->execute(array('id' => $id));
      $author = $req->fetch();
if($author){ 
      return new Author($author['author_id'], $author['author'], $author['authorbio']);
    }
    else
    {  echo 'this is your issue';
        //replace with a more meaningful exception
        throw new Exception('A real exception should go here');
    }
    }

public static function update($id) {
    $db = Db::getInstance();
    $req = $db->prepare("Update author set author=:author, authorbio=:authorbio where author_id=:id");
    $req->bindParam('id', $id);
    $req->bindParam('author', $author);
    $req->bindParam('authorbio', $authorbio);

    //set name and price parameters and execute
    if(isset($_POST['author'])&& $_POST['author']!=""){
        $filteredauthor = filter_input(INPUT_POST,'author', FILTER_SANITIZE_SPECIAL_CHARS);
    }
    if(isset($_POST['authorbio'])&& $_POST['authorbio']!=""){
       $filteredauthorbio= filter_input(INPUT_POST,'authorbio', FILTER_SANITIZE_SPECIAL_CHARS);
   }
$author = $filteredauthor;
$authorbio = $filteredauthorbio;
$req->execute();

//upload product image if it exists
        if (!empty($_FILES[self::InputKey]['name'])) {
                $name=$author;
		Author::uploadFile($name);
	}

    }
    
    public static function add() {
    $db = Db::getInstance();
    $req = $db->prepare("Insert into author (author, authorbio) values (:author, :authorbio)");
    $req->bindParam(':author', $author);
    $req->bindParam(':authorbio', $authorbio);

// set parameters and execute
    if(isset($_POST['author'])&& $_POST['author']!=""){
        $filteredAuthor = filter_input(INPUT_POST,'author', FILTER_SANITIZE_SPECIAL_CHARS);
    }
    if(isset($_POST['authorbio'])&& $_POST['authorbio']!=""){
        $filteredAuthorbio = filter_input(INPUT_POST,'authorbio', FILTER_SANITIZE_SPECIAL_CHARS);
    }
$author = $filteredAuthor;
$authorbio = $filteredAuthorbio;
$req->execute();
    
//upload product image

Author::uploadFile($author);
    }

const AllowedTypes = ['image/jpeg', 'image/jpg', 'image/png'];
const InputKey = 'myUploader';

//die() function calls replaced with trigger_error() calls
//replace with structured exception handling
public static function uploadFile($author) {

	if (empty($_FILES[self::InputKey])) {
		//die("File Missing!");
                trigger_error("File Missing!");
	}

	if ($_FILES[self::InputKey]['error'] > 0) {
		trigger_error("Handle the error! " . $_FILES[InputKey]['error']);
	}


	if (!in_array($_FILES[self::InputKey]['type'], self::AllowedTypes)) {
		trigger_error("Handle File Type Not Allowed: " . $_FILES[self::InputKey]['type']);
	}

	$tempFile = $_FILES[self::InputKey]['tmp_name'];
        $filepath = "views/images/author/";
	$destinationFile = $filepath .str_replace(' ','',$author).'.jpg';
        //$path = "C:/xampp/htdocs/finalproject_blog/views/images/author/";  this is the pc file

	if (!move_uploaded_file($tempFile, $destinationFile)) {
		trigger_error("Handle Error");
	}
		
	//Clean up the temp file
	if (file_exists($tempFile)) {
		unlink($tempFile); 
	}
}

public static function remove($id) {
      $db = Db::getInstance();
      //make sure $id is an integer
      $id = intval($id);
      $req = $db->prepare('delete FROM author WHERE author_id = :id');
      // the query was prepared, now replace :id with the actual $id value
      $req->execute(array('id' => $id));
  }
  
}

 class Member {
// OKAY THIS IS THE BIT THAT WORKS WITH ALL THE SQL. SO YOU GOT YOUR CREATE ADD DELETE READ ETC CLASSES
// 
    // we define 3 attributes
    public $id;
    public $email;
    public $name;

    public function __construct($id, $email, $name) {
      $this->id    = $id;
      $this->email  = $email;
      $this->name = $name;
    }

    public static function all() {
      $list = [];
      $db = Db::getInstance();
      $req = $db->query('SELECT * FROM member');
      // we create a list of Product objects from the database results
      foreach($req->fetchAll() as $product) {
        $list[] = new Product($product['id'], $product['email'], $product['name']);
      }
      return $list;
    }

    public static function find($id) {
        
      $db = Db::getInstance();
      //use intval to make sure $id is an integer
      $id = intval($id);
      $req = $db->prepare('SELECT * FROM member WHERE author_id = :id');
      //the query was prepared, now replace :id with the actual $id value
      $req->execute(array('id' => $id));
      $member = $req->fetch();
if($member){
      return new Member($member['id'], $member['email'], $member['name']);
    }
    else
    {
        //replace with a more meaningful exception
        throw new Exception('A real exception should go here');
    }
    }

public static function update($id) {
    $db = Db::getInstance();
    $req = $db->prepare("Update member set email=:email, name=:name where id=:id");
    $req->bindParam(':id', $id);
    $req->bindParam(':email', $email);
    $req->bindParam(':name', $name);

// set name and price parameters and execute
    if(isset($_POST['email'])&& $_POST['email']!=""){
        $filteredEmail = filter_input(INPUT_POST,'email', FILTER_SANITIZE_SPECIAL_CHARS);
    }
    if(isset($_POST['name'])&& $_POST['name']!=""){
        $filteredName= filter_input(INPUT_POST,'name', FILTER_SANITIZE_SPECIAL_CHARS);
    }
$email = $filteredEmail;
$name = $filteredName;
$req->execute();

//upload product image if it exists
        if (!empty($_FILES[self::InputKey]['name'])) {
		Member::uploadFile($name);
	}

    }
    
    public static function add() {
    $db = Db::getInstance();
    $req = $db->prepare("Insert into member(email, name) values (:email, :name)");
    $req->bindParam(':email', $email);
    $req->bindParam(':name', $name);

// set parameters and execute
    if(isset($_POST['email'])&& $_POST['email']!=""){
        $filteredEmail = filter_input(INPUT_POST,'email', FILTER_SANITIZE_SPECIAL_CHARS);
    }
    if(isset($_POST['name'])&& $_POST['name']!=""){
        $filteredName = filter_input(INPUT_POST,'name', FILTER_SANITIZE_SPECIAL_CHARS);
    }
$email = $filteredEmail;
$name = $filteredName;
$req->execute();

//upload product image
  Author::uploadFile($name);
    }

const AllowedTypes = ['image/jpeg', 'image/jpg'];
const InputKey = 'myUploader';

//die() function calls replaced with trigger_error() calls
//replace with structured exception handling
public static function uploadFile(string $name) {

	if (empty($_FILES[self::InputKey])) {
		//die("File Missing!");
                trigger_error("File Missing!");
	}

	if ($_FILES[self::InputKey]['error'] > 0) {
		trigger_error("Handle the error! " . $_FILES[InputKey]['error']);
	}


	if (!in_array($_FILES[self::InputKey]['type'], self::AllowedTypes)) {
		trigger_error("Handle File Type Not Allowed: " . $_FILES[self::InputKey]['type']);
	}

	$tempFile = $_FILES[self::InputKey]['tmp_name'];
        $path = "/views/images/author";
	$destinationFile = $path .str_replace(' ','', $author). '.jpeg';

	if (!move_uploaded_file($tempFile, $destinationFile)) {
		trigger_error("Handle Error");
	}
		
	//Clean up the temp file
	if (file_exists($tempFile)) {
		unlink($tempFile); 
	}
}
public static function remove($id) {
      $db = Db::getInstance();
      //make sure $id is an integer
      $id = intval($id);
      $req = $db->prepare('delete FROM member WHERE member_id = :id');
      // the query was prepared, now replace :id with the actual $id value
      $req->execute(array('id' => $id));
  }
  
}
class LogIn {
// OKAY THIS IS THE BIT THAT WORKS WITH ALL THE SQL. SO YOU GOT YOUR CREATE ADD DELETE READ ETC CLASSES
// 
    // we define 3 attributes
    public $id;
    public $username;
    public $password;
    public $admin;

    public function __construct($id, $username, $password, $admin) {
      $this->id    = $id;
      $this->username  = $username;
      $this->password = $password;
      $this->admin = $admin;
    }

    public static function all() {
      $list = [];
      $db = Db::getInstance();
      $req = $db->query('SELECT * FROM userid');
      // we create a list of Product objects from the database results
      foreach($req->fetchAll() as $userid) {
        $list[] = new Userid ($userid['id'], $userid['username'], $userid['password'], $userid['admin']);
      }
      return $list;
    }

    public static function find($id) {
        
      $db = Db::getInstance();
      //use intval to make sure $id is an integer
      $id = intval($id);
      $req = $db->prepare('SELECT * FROM userid WHERE id = :id');
      //the query was prepared, now replace :id with the actual $id value
      $req->execute(array('id' => $id));
      $userid = $req->fetch();
if($userid){
      return new User ($userid['id'], $userid['username'], $userid['password'], $userid['admin']);
    }
    else
    {
        //replace with a more meaningful exception
        throw new Exception('A real exception should go here');
    }
    }

public static function update($id) {
    $db = Db::getInstance();
    $req = $db->prepare("Update userid set username=:username, password=:password, admin=:admin, where id=:id");
    $req->bindParam(':id', $id);
    $req->bindParam(':username', $username);
    $req->bindParam(':password', $password);
    $req->bindParam(':admin', $admin);

// set name and price parameters and execute
    if(isset($_POST['username'])&& $_POST['username']!=""){
        $filteredUsername = filter_input(INPUT_POST,'username', FILTER_SANITIZE_SPECIAL_CHARS);
    }
    if(isset($_POST['password'])&& $_POST['password']!=""){
        $filteredPassword= filter_input(INPUT_POST,'password', FILTER_SANITIZE_SPECIAL_CHARS);
    }
        if(isset($_POST['admin'])&& $_POST['admin']!=""){
        $filteredAdmin= filter_input(INPUT_POST,'admin', FILTER_SANITIZE_SPECIAL_CHARS);
    }
$username = $filteredUsername;
$password = $filteredPassword;
$admin = $filteredAdmin;
$req->execute();

    }
    
    public static function add() {
    $db = Db::getInstance();
    $req = $db->prepare("Insert into userid (username, password, admin) values (:username, :password, :admin)");
    $req->bindParam(':username', $username);
    $req->bindParam(':password', $password);
    $req->bindParam(':admin', $admin);

// set parameters and execute
    if(isset($_POST['username'])&& $_POST['password']!=""){
        $filteredUsername = filter_input(INPUT_POST,'username', FILTER_SANITIZE_SPECIAL_CHARS);
    }
    if(isset($_POST['password'])&& $_POST['password']!=""){
        $filteredPassword = filter_input(INPUT_POST,'password', FILTER_SANITIZE_SPECIAL_CHARS);
    }
     if(isset($_POST['admin'])&& $_POST['admin']!=""){
        $filteredAdmin = filter_input(INPUT_POST,'admin', FILTER_SANITIZE_SPECIAL_CHARS);
    }
       
$username = $filteredUsername;
$password = $filteredPassword;
$admin = $filteredAdmin;
$req->execute();

}
public static function remove($id) {
      $db = Db::getInstance();
      //make sure $id is an integer
      $id = intval($id);
      $req = $db->prepare('delete FROM userid WHERE id = :id');
      // the query was prepared, now replace :id with the actual $id value
      $req->execute(array('id' => $id));
  }
  
}

  class Comment {
// OKAY THIS IS THE BIT THAT WORKS WITH ALL THE SQL. SO YOU GOT YOUR CREATE ADD DELETE READ ETC CLASSES
// 
    // we define 3 attributes
    public $id;
    public $uid;
    public $date;
    public $message;
    public $article_id;

    public function __construct($id, $uid, $date,$message, $article_id) {
      $this->id    = $id;
      $this->uid  = $uid;
      $this->date = $date;
      $this->message = $message;
      $this->article_id =$article_id;
    }

    public static function all() {
      $list = [];
      $db = Db::getInstance();
      $req = $db->query('SELECT * FROM comments');
      // we create a list of Product objects from the database results
      foreach($req->fetchAll() as $comment) {
        $list[] = new Comment($comment['cid'], $comment['uid'], $comment['date'], $comment['message'], $comment['article_id']);
      }
      return $list;
    }

    public static function find($id) {
        
      $db = Db::getInstance();
      //use intval to make sure $id is an integer
      $id = intval($id);
      $req = $db->prepare('SELECT * FROM comments WHERE cid = :id');
      //the query was prepared, now replace :id with the actual $id value
      $req->execute(array('id' => $id));
      $comment = $req->fetch();
if($comment){ 
      return new Comment($comment['cid'], $comment['uid'], $comment['date'],$comment['message'], $comment['article_id']);
    }
    else
    {  echo 'this is your issue';
        //replace with a more meaningful exception
        throw new Exception('A real exception should go here');
    }
    }

public static function update($id) {
    $db = Db::getInstance();
    $req = $db->prepare("Update comments set uid=:uid, date=:date, message=:message, article_id=:article_id where cid=:id");
    $req->bindParam('id', $id);
    $req->bindParam('uid', $uid);
    $req->bindParam('date', $date);
    $req->bindParam('message', $message);
    $req->bindParam('article_id',$article_id);

    //set name and price parameters and execute
        
    if(isset($_POST['uid'])&& $_POST['uid']!=""){
        $filtereduid = filter_input(INPUT_POST,'uid', FILTER_SANITIZE_SPECIAL_CHARS);    
    }
    if(isset($_POST['date'])&& $_POST['date']!=""){
       $filtereddate= filter_input(INPUT_POST,'date', FILTER_SANITIZE_SPECIAL_CHARS);
       }
    if(isset($_POST['message'])&& $_POST['message']!=""){
        $filteredmessage = filter_input(INPUT_POST,'message', FILTER_SANITIZE_SPECIAL_CHARS);
    }  
    if(isset($_POST['article_id'])&& $_POST['article_id']!=""){
        $filteredarticle_id = filter_input(INPUT_POST,'article_id', FILTER_SANITIZE_SPECIAL_CHARS);
    }
$uid = $filtereduid;
$date = $filtereddate;
$message = $filteredmessage;
$article_id= $filteredarticle_id;
        
$req->execute();

}
    
    public static function add() {
    $db = Db::getInstance();
    $req = $db->prepare("Insert into comments (uid, date, message, article_id ) values (:uid, :date, :message, :article_id )");
    $req->bindParam(':uid', $uid);
    $req->bindParam(':date', $date);
    $req->bindParam(':message', $message);
    $req->bindParam(':article_id', $article_id);

// set parameters and execute
   if(isset($_POST['uid'])&& $_POST['uid']!=""){
        $filtereduid = filter_input(INPUT_POST,'uid', FILTER_SANITIZE_SPECIAL_CHARS);    
    }
    if(isset($_POST['date'])&& $_POST['date']!=""){
       $filtereddate= filter_input(INPUT_POST,'date', FILTER_SANITIZE_SPECIAL_CHARS);
       }
    if(isset($_POST['message'])&& $_POST['message']!=""){
        $filteredmessage = filter_input(INPUT_POST,'message', FILTER_SANITIZE_SPECIAL_CHARS);
    }  
    if(isset($_POST['article_id'])&& $_POST['article_id']!=""){
        $filteredarticle_id = filter_input(INPUT_POST,'article_id', FILTER_SANITIZE_SPECIAL_CHARS);
    }
$uid = $filtereduid;
$date = $filtereddate;
$message = $filteredmessage;
$article_id= $filteredarticle_id;
$req->execute();
}   
//upload product image



public static function remove($id) {
      $db = Db::getInstance();
      //make sure $id is an integer
      $id = intval($id);
      $req = $db->prepare('delete FROM comments WHERE cid = :id');
      // the query was prepared, now replace :id with the actual $id value
      $req->execute(array('id' => $id));
  }
  
}