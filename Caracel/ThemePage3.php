<!DOCTYPE html>
<html lang="en">
<head>
  <title>Search Themes</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
  
  <style> 
  .carousel-inner img {
      width: 80%; /* Set width to 100% */
      margin: auto;
      min-height:80px;
  }

  /* Hide the carousel text when the screen is less than 600 pixels wide */
  @media (max-width: 600px) {
    .carousel-caption {
      display: none; 
    }
  }
  </style>
</head>
<body>


<div id="myCarousel" class="carousel slide" data-ride="carousel">
  <!-- Indicators -->
  <ol class="carousel-indicators">
    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
    <li data-target="#myCarousel" data-slide-to="1"></li>
    <li data-target="#myCarousel" data-slide-to="2"></li>
  </ol>

  <!-- Wrapper for slides -->
  <div class="carousel-inner">
      
   
      <div class="item active">
          <a href="catPages/Homepage_million_stories.php">
        <img id='Btech' class="center-block" src="Images/Homepage_million_stories.jpg" alt="B.tech">
        
        
      <div class="carousel-caption">
        <h3>B.Tech</h3>
        <p>Hear form women, that have forged the path in technology!!</p>
      </div> </a>
    </div>

    <div class="item">
        <a href="catPages/Byourself.php">
        <img class="center-block" src="Images/Byourself.jpg" alt="B.yourself" id='Byourself' >
      <div class="carousel-caption">
        <h3>B.Yourself</h3>
        <p>Be inspired to be the best you!!</p>
      </div> </a>
    </div>

    <div class="item">
        <a href="catPages/Bcreative.php">
        <img class="center-block" src="Images/creative.2.jpg" alt="B.creative" width='80%' id='Bcreative' >
      <div class="carousel-caption">
        <h3>B.Creative</h3>
        <p>Think BIG!!</p>
      </div> </a>
    </div>
  </div>

  <!-- Left and right controls -->
  <a class="left carousel-control" href="#myCarousel" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="right carousel-control" href="#myCarousel" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right"></span>
    <span class="sr-only">Next</span>
  </a>
</div>


  

</body>
</html>
