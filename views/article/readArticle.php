<!DOCTYPE html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="../css/BootstrapCSS.css" rel="stylesheet" type="text/css"/>
</head>

<body>
    <div class=" p-6 rounded bg-white mainarticle">
    <div class="row p-6 align-items-center">
    <div class=" offset-md-1 col-md-6 p-6 boxed">
        
            <div class=" p-6 heading">
                <div class=" display-3 align-items-center title">
                    <h1> <?php echo $product->title; ?></h1>
                </div>
                <div class="subheading">
                    <!-- Put in the summary and subheading here-->
                </div>
                <div class="author">
                    <row> 
                                    <?php $file = 'views/images/author/' .str_replace(' ', '',$product->author).'.jpg';
                                    if(file_exists($file)){
                                    $img = "<img class='rounded-circle' src='$file' width='50' height='50' />";
                                    echo $img;
                                    }
                                    else
                                    {
                                    echo "<img class=rounded-circle' src='views/images/author/noprofile.jpg' width='50' height='50' />";
                                    }
                                    ?>  
                        <a href="#"> <?php echo $product->author; ?></a><br>
                    </row>
                </div>
                <div class="pubdate">
                    <?php echo date("d M y",strtotime($product->date)); ?>
                </div><br>
            </div>
    </div>       
            <div class="col-md-4  image">
      <?php        
      $file = 'views/images/article/'.str_replace(' ', '',$product->title).$product->fk_author.'.jpg';
if(file_exists($file)){
    $img = "<img class='rounded m-2 p-6' src='$file' width='400' height= ':auto'/>";
echo $img;}
    
?>
        </div>
        </div>        
         
        <div class="p-6 row">  
            <div class=" col-md-10 offset-md-1 p-4 content">
                <p><?php echo html_entity_decode($product->cont); ?></p>
                
            </div> 
        </div>
        </div> 
     </div>
     <br>
<!-- comments container-->
 
<!-- comments container-->

 <?php require 'views/pages/commentsview.php'?>
</body>            
            
   