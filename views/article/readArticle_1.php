<!DOCTYPE html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="../css/BootstrapCSS.css" rel="stylesheet" type="text/css"/>
</head>

<body>
    <div class=" col-md-10 offset-md-1 p-6 rounded bg-white mainarticle">
    <div class="row align-items-center">
    <div class=" offset-md-1 col-md-6 p-6 boxed">
        
            <div class="heading">
                <div class=" display-3 align-items-center title">
                    <h1> <?php echo $product->title; ?></h1>
                </div>
                <div class="subheading">
                    <!-- Put in the summary and subheading here-->
                </div>
                <div class="author">
                    <a href="#"> <?php echo $product->author; ?></a><br>
                </div>
                <div class="pubdate">
                    <?php echo date("d M y",strtotime($product->date)); ?>
                </div><br>
            </div>
    </div>       
            <div class="col-md-4  image">
      <?php        
      $file = 'views/images/article/'.str_replace(' ', '',$product->title).$product->fk_author.'.jpg';
if(file_exists($file)){
    $img = "<img class='rounded m-2 p-3' src='$file' width='300' height= ':auto'/>";
echo $img;}
    
?>
        </div>
        </div>        
         
            
            <div class="content">
                <p><?php echo html_entity_decode($product->cont); ?></p>
                
            </div> 
        </div> 
     </div>

<!-- comments container-->
 
<!-- comments container-->

 <?php require 'views/pages/commentsview.php'?>
</body>            
            
   