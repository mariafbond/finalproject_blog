<h1>Comment Admin Table</h1>
<div id="admin table" >
  <table>
      <tr>
        
        <th>id</th>
        <th>Username</th>
        <th>date</th>
        <th>message</th>
        <th>article ID</th>
        <th>action</th>
        
        
      </tr>
  <?php
  /* FetchAll foreach with edit and delete using Ajax */

   foreach($comments as $comment){ ?>
     <tr>
       <td><?php echo $comment->id; ?></td>  
       <td><?php echo $comment->uid; ?></td>
       <td><?php echo $comment->date; ?></td>
       <td><?php echo substr(strip_tags(html_entity_decode($comment->message)),0,500).'...'; ?></td>
       <td><?php echo $comment->article_id; ?></td>
      
       <td><a  class='editbtn'  href="?controller=admin&action=commentUpdate&id=<?php echo $comment->id; ?>">Edit Record</a>&nbsp;|&nbsp;
           <a  class= 'delbtn' href= "?controller=admin&action=commentDelete&id=<?php echo $comment->id; ?>">Delete</a></td>
     </tr> 
   <?php } ?>
  </table>
       </div>