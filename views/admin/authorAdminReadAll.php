
<h1>Author Admin Table</h1>
<div id="admin table" >
  <table>
      <tr>
        
        <th>id</th>
        <th>Author</th>
        <th>AuthorBio</th>
        
        
      </tr>
  <?php
  /* FetchAll foreach with edit and delete using Ajax */

   foreach($authors as $author){ ?>
     <tr>
       <td><?php echo $author->id; ?></td>
       <td><?php echo $author->author; ?></td>
       <td><?php echo substr(strip_tags(html_entity_decode($author->authorbio)),0,120).'...'; ?></td>
       
      
       <td><a  class='editbtn'  href="?controller=author&action=update&id=<?php echo $author->id; ?>">Edit Record</a>&nbsp;|&nbsp;
           <a  class= 'delbtn' href= "?controller=admin&action=authorDelete&id=<?php echo $author->id; ?>">Delete</a></td>
     </tr> 
   <?php } ?>
  </table>
       </div>

<div>
    <a href="?controller=author&action=create" ><button type="button" class="btn btn-info btn-lg">create author</button></a>
</div>