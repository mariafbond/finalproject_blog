<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        
        <meta charset="UTF-8">
        
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link href="../views/css/BootstrapCSS.css" rel="stylesheet" type="text/css"/>
    <link href="https://fonts.googleapis.com/css?family=Karla|Special+Elite" rel="stylesheet">
    <link rel="icon" href="views/images/authorimages/women_empowered.jpg">
    <script src="jquery/jquery-3.3.1.js" type="text/javascript"></script>
        
        <title></title>
    </head>
    <body>
        <div class="container">
        <?php
        require "../../connection.php";
        require "../../Model/database.php";
        require "../../controllers/AdminController.php";
        ?>
        
        <h1> Article Administration page </h1>
        
    <div class="row">  
        <div class="col">
            <div id=adminreadall>
                <?php require '../../controllers/articleAdminTable.php'?>
            </div>
        </div> 
        
        <div class="col"> 
            <div id='createeditadmin'>
            <?php require 'externalArticleCreateUpdate.php'?>
            </div>
        </div>    
    </div>  
    </div> 
        
       
  <!-- ed of body javascripty stuff-->      
     <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery-slim.min.js"><\/script>');</script>
    <script src="../../assets/js/vendor/popper.min.js"></script>
    <script src="../../dist/js/bootstrap.min.js"></script>
    <script src="../../assets/js/vendor/holder.min.js"></script>
    <script>
      Holder.addTheme('thumb', {
        bg: '#55595c',
        fg: '#eceeef',
        text: 'Thumbnail'
      });
    </script>
  <!--- this stops search function working  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>-->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
   <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
   <!-- Tinymce script stuff -->
   <script src="https://cloud.tinymce.com/5/tinymce.min.js"></script>
        <script>tinymce.init({ selector:'textarea.wysiwyg', height: '350px'});</script>     
    
    </body>
</html>
