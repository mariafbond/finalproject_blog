<!DOCTYPE html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="../css/BootstrapCSS.css" rel="stylesheet" type="text/css"/>
    <style>

.img1 {
  float: right;
}

.clearfix {
  overflow: auto;
}

</style>
</head>

<div class=" col-lg-10 offset-md-1 authorbio">
    <div class="boxed">
        <div class="abio">
            <div class="heading">
                <div class="name"></div>
                                    
                
                <div class="author">
                    <h1><a href="#"> <?php echo $author->author; ?></a></h1>
                </div>
            </div>
            <div class="content">
                <h3><?php   echo html_entity_decode($author->authorbio); ?></h3>   
            </div>
        </div>
        <div class="clearfix">  
            <div  class="img1"><?php $file = 'views/images/author/' .str_replace(' ', '',$author->author).'.jpg';
if(file_exists($file)){
    $img = "<img class='rounded align-self-center' src='$file' width='300' />";
    echo $img;
}
else
{
echo "<img src='views/images/author/noprofile.jpg' width='300' />";
}
?>     </div>      
        </div> 
      </div> 
