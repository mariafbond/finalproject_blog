<!--Featured articles-->


      <div class="row mb-2 align-content-stretch">
          
        <div class="col-md-6">
          <div class="card flex-md-row mb-4 box-shadow h-md-250">
            <div class="card-body d-flex flex-column align-items-start">            
              <strong class="d-inline-block mb-2 text-primary">B.Yourself</strong>
             
              <h3 class="mb-0">
                <a class="text-dark" href="?controller=article&action=readAll">Celebrating Women's Day</a>
              </h3>
              <div class="mb-1 text-muted">Mar 7</div>
              <p class="card-text mb-auto">The award-winning author, screenwriter and columnist Caitlin Moran visited Sky to talk</p>
              <a href="nbproject/bYourself.php">Continue reading</a>
            </div>
              <img class="card-img-right flex-auto d-none d-md-block align-self-center img-fluid" src="views/images/author/caitlin.jpg" width="38.2%"height=":auto"  alt="">
            
          </div>
        </div>
          
        <div class="col-md-6">
          <div class="card flex-md-row mb-4 box-shadow h-md-250">
            <div class="card-body d-flex flex-column align-items-start">
              <strong class="d-inline-block mb-2 text-primary">B.Tech</strong>
              <h3 class="mb-0">
                <a class="text-dark" href="#">Post title</a>
              </h3>
              <div class="mb-1 text-muted">Nov 11</div>
              <p class="card-text mb-auto">This is a wider card with supporting text below as a natural lead-in to additional content.</p>
              <a href="#">Continue reading</a>
            </div>
              <img class="card-img-right flex-auto d-none d-md-block align-self-center img-fluid" src="views/images/creative.jpg" width="38.2%"height=":auto"  alt=""
          </div>
        </div>
      </div>
    </div>

    <main role="main" class="container">
      <div class="row">
        <div class="col-md-8 blog-main">
          <h3 class="pb-3 mb-4 font-italic border-bottom">
            From the Firehose
          </h3>
            
            <?php require_once ('"../../controllers/homeArticleDiv.php"');?>
          
         <!-- <div class="blog-post">
            <h2 class="blog-post-title"></h2>
            <p class="blog-post-meta"><a class="authorlink" href="#"></a></p>
            <p></p>
          </div><!-- /.blog-post -->
         <!--
          <div class="blog-post">
            <h2 class="blog-post-title">Another blog post</h2>
            <p class="blog-post-meta">December 23, 2013 by <a href="#">Jacob</a></p>

            <p>Cum sociis natoque penatibus et magnis <a href="#">dis parturient montes</a>, nascetur ridiculus mus. Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum. Sed posuere consectetur est at lobortis. Cras mattis consectetur purus sit amet fermentum.</p>
            <blockquote>
              <p>Curabitur blandit tempus porttitor. <strong>Nullam quis risus eget urna mollis</strong> ornare vel eu leo. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
            </blockquote>
            <p>Etiam porta <em>sem malesuada magna</em> mollis euismod. Cras mattis consectetur purus sit amet fermentum. Aenean lacinia bibendum nulla sed consectetur.</p>
            <p>Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Morbi leo risus, porta ac consectetur ac, vestibulum at eros.</p>
          </div><!-- /.blog-post -->
          <!--
          <div class="blog-post">
            <h2 class="blog-post-title">New feature</h2>
            <p class="blog-post-meta">December 14, 2013 by <a href="#">Chris</a></p>

            <p>Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Aenean lacinia bibendum nulla sed consectetur. Etiam porta sem malesuada magna mollis euismod. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.</p>
            <ul>
              <li>Praesent commodo cursus magna, vel scelerisque nisl consectetur et.</li>
              <li>Donec id elit non mi porta gravida at eget metus.</li>
              <li>Nulla vitae elit libero, a pharetra augue.</li>
            </ul>
            <p>Etiam porta <em>sem malesuada magna</em> mollis euismod. Cras mattis consectetur purus sit amet fermentum. Aenean lacinia bibendum nulla sed consectetur.</p>
            <p>Donec ullamcorper nulla non metus auctor fringilla. Nulla vitae elit libero, a pharetra augue.</p>
          </div><!-- /.blog-post --> 
           <!--lala-->
          <nav class="blog-pagination">
            <a class="btn btn-outline-primary" href="#">Older</a>
            <a class="btn btn-outline-secondary disabled" href="#">Newer</a>
          </nav>

        </div><!-- /.blog-main -->

        <!--Margin About-->
        
        <aside class="col-md-4 blog-sidebar">
          <div class="p-3 mb-3 bg-light rounded">
            <h4 class="font-italic">About</h4>
            <p class="mb-0">Etiam porta <em>sem malesuada magna</em> mollis euismod. Cras mattis consectetur purus sit amet fermentum. Aenean lacinia bibendum nulla sed consectetur.</p>
         <div class="p-3">
             <br>
            <h4 class="font-italic">Share your story</h4>
            <ol class="list-unstyled mb-0">
              <li><a href="#">Submit an article</a></li>
              <li><a href="#">Contact us</a></li>
              <li><a href="#">Sign up to our newsletter</a></li>
            </ol>
            <br>
            <h4 class="font-italic">Our favourite sites</h4>
            <ol class="list-unstyled">
              <li><a href="http://getintotech.sky.com/">Sky Get into Tech</a></li>
              <li><a href="https://www.monster.com/career-advice/article/career-planning-tips">Career Planning</a></li>
              <li><a href="https://www.the-pool.com/">The Pool</a></li>
            </ol>
          </div>
          </div>
          
        </aside><!-- /.blog-sidebar -->

      </div><!-- /.row -->
      
    </main><!-- /.container -->