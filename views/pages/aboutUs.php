<!DOCTYPE html>
<html lang="en">
<head>
  <title>About Us</title>
  <meta charset="utf-8">

  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
<meta name="viewport" content="width=device-width, initial-scale=1">
<style>
    body, html {
  height: 100%;
  margin: 0;
  font-family: Arial, Helvetica, sans-serif;
}

.hero-image {
  background-image: linear-gradient(rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.5)), url("../images/AboutUs/hands2.jpg");
  height: 50%;
  background-position: center;
  background-repeat: no-repeat;
  background-size: cover;
  position: relative;
}

.hero-text {
  text-align: center;
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  color: white;
}

</style>
<style>
ul {
  list-style-type: none;
  margin: 0;
  padding: 0;
  overflow: hidden;
  background-color: #333;
}

li {
  float: left;
}

li a {
  display: block;
  color: white;
  font-size: 1.6em  ;
  text-align: center;
  padding: 16px 18px;
  text-decoration: none;
}

li a:hover {
  background-color: #111;
}

</style>
</head>
<body>
    
<div class="hero-image">
  <div class="hero-text">
    <h2 style="font-size:50px">All About Us</h2>
    <p>Meet the members of Team Codettes</p>
   
  </div>
</div>
 <div class="container">
   
    
      <div class="nav-scroller pb-1 mb-2">
        <nav class="nav d-flex justify-content-between">
                      <ul>

        <li>  <a class="p-2 text-muted" href="/FinalProject_Blog/index.php">Home</a></li>
         <li> <a class="p-2 text-muted" id="b-tech" href="/FinalProject_Blog/index.php?controller=article&action=theme&id=1">B.Tech</a></li>
          <li><a class="p-2 text-muted" id="b-you" href="/FinalProject_Blog/index.php?controller=article&action=theme&id=2">B.Yourself</a></li>
         <li> <a class="p-2 text-muted" id="b-create" href="/FinalProject_Blog/index.php?controller=article&action=theme&id=3">B.Creative</a></li>
         <li> <a class="p-2 text-muted" href="/FinalProject_Blog/index.php/views/pages/Login.php">Log in</a>
                      </ul></nav>
      </div>
        
   
<div class="container">
  <div class="row">
    <div class="col-sm-4">
         <img src="../images/AboutUs/Maria.jpg" alt="Maria" style="width:350px;height:350px;" />
     
     
      
    </div>
    <div class="col-sm-8">
      <h3>Maria's Story</h3>
      <p>I was approaching my 10 year anniversary at Sky when I found out about the Get Into Tech programme.  At the moment I manage the today@livingston page using CMS and I thought that this course would give me the skills to be able to add features to the site that would be impressive.  I worked with the admins for today@dunfermline.  They had coding experience and produced a lot of things such as a briefing tracker that they shared with me.  I wanted to be able to bring the same skills and talent to today@livingston.

The course has been more intense than I originally thought it would be and I did not expect to learn so much.  I thought we would be learning how to code and building some simple programmes.

I have learnt a lot and going forward I would like to pursue a career in the Technology sector..</p>
   
    </div>
    
  </div>
</div>

    
    <div class="container">
  <div class="row">
    <div class="col-sm-8">
         <h3>Caroline's Story</h3>
      <p>I had been in a tech role in CRT team before I had gone off on MAT leave. When this role ended, I went back to my old job in ORT. I was looking for opportunity to develop myself and get a career kick started in a new department. When I saw the course advertised, I went and applied strait away. The more I learned about the course, and software development the more I wanted to do it. I said to my partner on the first day of training, this feels like the 1st day of the rest of my life! </p>
        
    </div>
    <div class="col-sm-4">
      <img src="../images/AboutUs/Me2.jpg" alt="Caroline" style="width:230px;height:300px;" />
   
    </div>
    
  </div>
</div>
    <div class="container">
  <div class="row">
    <div class="col-sm-4">
        <img src="../images/AboutUs/tinkerbell.jpg" alt="" style="width:350px;height:350px;" />
     
     
      
    </div>
    <div class="col-sm-8">
      <h3>Anna's Story</h3>
      <p> I have worked for sky for just under 5 years. I have a background in creative arts and design and I was alway the kind of person that thought somehow because I was a 'creative' person that coding was not for me even though when I look back now I can see how it fits my creative and intellectual sides. I have always been a problem solver, I love to learn and a nice juicy task just makes me happy. As soon as I saw the advert for Get into Tech I knew I would have to go for it and I am so glad I did.</p>
      <p> We have managed to cover a huge amount in the last 16 weeks and I love that it is starting to all fit together. I find it so exciting now as I can now see how all the strands of knowledge are starting to mesh together. It great when I manage to make something I have half imagined work and function. 
          My ambition after this course is to keep on coding, I feel like I have found something that I have a talent for. </p>
   
    </div>
    
  </div>
</div>

    
    <div class="container">
  <div class="row">
    <div class="col-sm-8">
         <h3>Joanna's Story</h3>
      <p>I work in the Continuous Improvement team where I help HR teams to analyse their processes, understand the root cause of their issues and design a solution. I was involved in several projects linked to different HR systems and I led the reimplementation of the HR content management system, Laserfiche. I have always been interested in ‘how the things work’ on the technical side and I’d like to progress my career in this direction. I also thought that our developers, who were the targets of my endless questions, deserved some rest:) The GIT course was a fantastic opportunity to learn the basics and meet some like-minded people. Although I feel I still have a long way to go before I’m a confident beginner coder, I have learnt a lot and I’m looking forward to more coding projects.</p>
        
    </div>
    <div class="col-sm-4">
        <img src="../images/AboutUs/Joanna.jpg" alt="Joanna" style="width:300px;height:350px;" />
   
    </div>
    
  </div>
</div>
     <div class="container">
  <div class="row">
    <div class="col-sm-4">
        <img src="../images/AboutUs/Natalia.jpeg" alt="" style="width:252px;height:244px;" />
     
     
      
    </div>
    <div class="col-sm-8">
      <h3>Natalia's Story</h3>
      <p>Within the digital world, new platforms are constantly emerging, transforming society and the economy. I want to be at the forefront of this innovation, working alongside talented individuals, designing and developing new systems and products. 
Get into Tech has supported my ambition by giving me the opportunity to learn coding and transform my career. It’s tough but invigorating. By joining Get into Tech you are empowered, I no longer think ‘if I can’ it is now ‘I will’ develop and create a technology. That is exciting. It all happened because of Sky, they created an incredible opportunity with an excellent level of support. It is not just about the skills you learn but the skills you learn but the friendships that you make. We encourage each other and together we can encourage others.</p>
   
    </div>
    
  </div>
</div>

    <!DOCTYPE html>
<html>
<meta name="viewport" content="width=device-width, initial-scale=1">
<style>
body {
  font-family: Arial;
  margin: 0;
}

* {
  box-sizing: border-box;
}

img {
  vertical-align: middle;
}

/* Position the image container (needed to position the left and right arrows) */
.container {
  position: relative;
}

/* Hide the images by default */
.mySlides {
  display: none;
}

/* Add a pointer when hovering over the thumbnail images */
.cursor {
  cursor: pointer;
}

/* Next & previous buttons */
.prev,
.next {
  cursor: pointer;
  position: absolute;
  top: 40%;
  width: auto;
  padding: 16px;
  margin-top: -50px;
  color: white;
  font-weight: bold;
  font-size: 20px;
  border-radius: 0 3px 3px 0;
  user-select: none;
  -webkit-user-select: none;
}

/* Position the "next button" to the right */
.next {
  right: 0;
  border-radius: 3px 0 0 3px;
}

/* On hover, add a black background color with a little bit see-through */
.prev:hover,
.next:hover {
  background-color: rgba(0, 0, 0, 0.8);
}

/* Number text (1/3 etc) */
.numbertext {
  color: #f2f2f2;
  font-size: 12px;
  padding: 8px 12px;
  position: absolute;
  top: 0;
}

/* Container for image text */
.caption-container {
  text-align: center;
  background-color: #222;
  padding: 2px 16px;
  color: white;
}

.row:after {
  content: "";
  display: table;
  clear: both;
}

/* Six columns side by side */
.column {
  float: left;
  width: 16.66%;
}

/* Add a transparency effect for thumnbail images */
.demo {
  opacity: 0.6;
}

.active,
.demo:hover {
  opacity: 1;
}
</style>
<body>

<h2 style="text-align:center">Team Work</h2>

<div class="container">
  <div class="mySlides">
    <div class="numbertext">1 / 6</div>
    <img src="../images/AboutUs/team.jpg" style="width:100%; hight:50%;">
  </div>

  <div class="mySlides">
    <div class="numbertext">2 / 6</div>
    <img src="../images/AboutUs/team1.jpg" style="width:100%; hight:50%;">
  </div>

  <div class="mySlides">
    <div class="numbertext">3 / 6</div>
    <img src="../images/AboutUs/team2.jpg" style="width:100%; hight:50%;">
  </div>
    
  <div class="mySlides">
    <div class="numbertext">4 / 6</div>
    <img src="../images/AboutUs/team3.jpg" style="width:100%; hight:50%;">
  </div>

  <div class="mySlides">
    <div class="numbertext">5 / 6</div>
    <img src="../images/AboutUs/team4.jpg" style="width:100%; hight:50%;">
  </div>
    
  <div class="mySlides">
    <div class="numbertext">6 / 6</div>
    <img src="../images/AboutUs/team5.jpg" style="width:100%; hight:50%;">
  </div>
    
  <a class="prev" onclick="plusSlides(-1)">?</a>
  <a class="next" onclick="plusSlides(1)">?</a>

  <div class="caption-container">
    <p id="caption"></p>
  </div>

  <div class="row">
    <div class="column">
      <img class="demo cursor" src="../images/AboutUs/team.jpg" style="width:100%" onclick="currentSlide(1)" alt="Be Bold: be proactive, make decisions, take responsibility, try new things.">
    </div>
    <div class="column">
      <img class="demo cursor" src="../images/AboutUs/team1.jpg" style="width:100%" onclick="currentSlide(2)" alt="Be Curious:  ask questions, do some research, learn new techniques.">
    </div>
    <div class="column">
      <img class="demo cursor" src="../images/AboutUs/team2.jpg" style="width:100%" onclick="currentSlide(3)" alt="Be Together: play an active role in the team, support your colleagues, collaborate, have fun.">
    </div>
    <div class="column">
      <img class="demo cursor" src="../images/AboutUs/team3.jpg" style="width:100%" onclick="currentSlide(4)" alt="Be Connected:  meet people, make contacts, build relationships, see the bigger picture.">
    </div>
    <div class="column">
      <img class="demo cursor" src="../images/AboutUs/team4.jpg" style="width:100%" onclick="currentSlide(5)" alt="Be Better: look for ways to improve, challenge yourself, never stop learning, strive to be the best.">
    </div>    
    <div class="column">
      <img class="demo cursor" src="../images/AboutUs/team5.jpg" style="width:100%" onclick="currentSlide(6)" alt="Building, developing, training, retaining and engaging the Codettes team is a daily commitment. We work hard every day to make sure that our bolg is amazing">
    </div>
  </div>
</div>

<script>
var slideIndex = 1;
showSlides(slideIndex);

function plusSlides(n) {
  showSlides(slideIndex += n);
}

function currentSlide(n) {
  showSlides(slideIndex = n);
}

function showSlides(n) {
  var i;
  var slides = document.getElementsByClassName("mySlides");
  var dots = document.getElementsByClassName("demo");
  var captionText = document.getElementById("caption");
  if (n > slides.length) {slideIndex = 1}
  if (n < 1) {slideIndex = slides.length}
  for (i = 0; i < slides.length; i++) {
      slides[i].style.display = "none";
  }
  for (i = 0; i < dots.length; i++) {
      dots[i].className = dots[i].className.replace(" active", "");
  }
  slides[slideIndex-1].style.display = "block";
  dots[slideIndex-1].className += " active";
  captionText.innerHTML = dots[slideIndex-1].alt;
}
</script>
    
</body>
</html>

</body>
</html>
