<!--Featured articles-->

      <div class="row mb-2">
          
        <div class="col-md-6">
          <div class="card flex-md-row mb-4 box-shadow h-md-250 align-content-start ">
              <?php require ('controllers/leftfeaturecontroller.php')?>
            
            
          </div>
        </div>
          
          
        <div class="col-md-6">
 
            <div class="card flex-md-row mb-4 box-shadow h-md-250 align-content-start">
               <?php require ('controllers/rightfeaturecontroller.php')?>         
            </div>
            
          </div>
        </div>
      
    

    <main role="main" class="container">
      <div class="row">
        <div class="col-md-8 blog-main" id='mainbody'>
          <h3 class="pb-3 my-4 font-bold border-bottom">
            Blogs from Inspirational Women
          </h3>
            
            <?php require ('controllers/homeArticleDiv.php')?>
          
        

          <nav class="blog-pagination">
            <a class="btn btn-info mb-4" href="?controller=article&action=readAll">All Articles</a>
            
            
          </nav>

        </div><!-- /.blog-main -->

        <!--Margin About-->
        
        <aside class="col-md-4 blog-sidebar float-right">
          <div class="p-3 mb-3 bg-light rounded float-right">
            <h4 class="">Welcome to 33 million stories</h4>
            <p class="mb-0">Showcasing inspiring women from around the world. We’re looking at women leading the way across all industries and sectors from creative, science and technology to health and wellbeing.</p>
            <div class="p-3">
                <h4 class="">About Us</h4>
            <p class="mb-0">We are a group of 5 independent women who were looking to gain inspiration from women who had broke through barriers to achieve success in their professional or personal lives.</p>
         <li><a href="views/pages/aboutUs.php">Find out a bit more about us</a></li>
            <div class="p-3">
             <br>
            <h4 class="">Share your story</h4>
            <ol class="list-unstyled mb-0">
              <li><a href="views/pages/newForm.php">Submit an article</a></li>
              <li><a href="views/pages/newForm.php">Contact us</a></li>
              <li><a href="views/pages/subscribe.php">Sign up to our newsletter</a></li>
            </ol>
            <br>
            <h4 class="">Our favourite sites</h4>
            <ol class="list-unstyled">
              <li><a href="http://getintotech.sky.com/">Sky Get into Tech</a></li>
              <li><a href="https://www.monster.com/career-advice/article/career-planning-tips">Career Planning</a></li>
              <li><a href="https://www.the-pool.com/">The Pool</a></li>
               <li><a href="https://www.ted.com/playlists/682/the_most_popular_ted_talks_of_2018">TED talks to inspire</a></li>
               
            </ol>
          </div>
          </div>
        <!--Margin Archives-->
        
         
        <!--Margin Sites-->
        
          
        </aside><!-- /.blog-sidebar -->

      </div><!-- /.row -->
      
    </main><!-- /.container -->
