<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link href="../css/newForm.css" rel="stylesheet" type="text/css"/>
    <link rel="icon" href="../images/authorimages/women_empowered.jpg">
<title>Website Contact Form</title>
<script>
function _(id){ return document.getElementById(id); }
function submitForm(){
	_("mybtn").disabled = true;
	_("status").innerHTML = 'please wait ...';
	var formdata = new FormData();
	formdata.append( "n", _("n").value );
	formdata.append( "e", _("e").value );
	formdata.append( "m", _("m").value );
	var ajax = new XMLHttpRequest();
	ajax.open( "POST", "example_parser.php" );
	ajax.onreadystatechange = function() {
		if(ajax.readyState == 4 && ajax.status == 200) {
			if(ajax.responseText == "success"){
				_("my_form").innerHTML = '<h2>Thanks '+_("n").value+', we will be in touch shortly.</h2>';
			} else {
				_("status").innerHTML = ajax.responseText;
				_("mybtn").disabled = false;
			}
		}
	}
	ajax.send( formdata );
}
</script>
</head>
<body>
    <!-- Navbar -->
<nav class="navbar navbar-default">
        <div class="nav-scroller pb-1 mb-2">
        <nav class="nav d-flex justify-content-between">
          <a href="/FinalProject_Blog/index.php" class="btn btn-default btn-lg">
    <span class="glyphicon glyphicon"></span> Home
  </a>

</nav>
    <div class="container">
    <form id="contact" onsubmit="submitForm(); return false;">
        <h3>Contact Us</h3>
        <h4>If you'd like to get in touch or share your story, please fill in the form. We'll respond within 48hrs.</h4>
    
        <fieldset>
            <p><input id="n" placeholder="Name" required></p>
        </fieldset>
        <fieldset>
            <p><input id="e" placeholder="Email Address" type="email" required></p>
        </fieldset>
        <fieldset>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="form_need">I want to... </label>
                    <select id="form_need" name="need" class="form-control"> <!--required="required" data-error="Please specify your need."-->
                        <option value=""></option>
                        <option value="Provide feedback">Provide feedback</option>
                        <option value="Ask a question">Ask a question</option>
                        <option value="Submit an article">Submit an article</option>
                        <option value="Other">Other</option>
                    </select>
                    <div class="help-block with-errors"></div>
                </div>
            </div>
        </fieldset>
        <fieldset>
            <textarea id="m" placeholder="write your message here" rows="10" required></textarea>
        </fieldset>
        <fieldset>
            <p><button name="mybtn" type="submit" value="Submit Form">SUBMIT</button><span id="status"></span></p>
        </fieldset>
    </form>
    </div>
            <!-- Footer -->
        <footer class="container-fluid bg-4 text-center">
        <p>&copy; Codettes 2019 &middot; <a href="priv.php">Privacy Policy</a>&middot;</p>
    </footer>
</body>
</html>