<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <title>Privacy Policy</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <meta charset="UTF-8">
        <link href="css/BootstrapCSS.css" rel="stylesheet" type="text/css"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        
    </head>
    <body>
        <div class="blog-post">
            <div class="jumbotron" id='themey'>
            <h5 class="blog-post-title">About Us</h5>
            <p>B.come – 33 million stories blog blog address (the Site) is owned by Codettes</p>
            <p>We can be contacted at codettes@mail.com</p>
            <p>Last updated: 5th March 2019</p>
            <hr>
            <p>This privacy Notice is here to inform you of our policies regarding the collection, use and disclosure of Personal Information we receive from users of the Site.
We use your Personal Information only for providing and improving the Site. By using the Site, you agree to the collection and use of information in accordance with this Privacy Policy.</p>
            <hr>
            <h5 class="blog-post-title">Information Collection & Use</h5>
            <p>While using our Site, we may ask you to provide us with certain personally identifiable information that can be used to contact or identify you. Personal identifiable information will only include your email address (to leave a comment or sign up for the update notifications) and your name (this is optional).
Like many site operators, we collect information that your browser sends whenever you visit our Site (“Log Data”).
This Log Data may include information such as your computer’s Internet Protocol (“IP”) address, browser type, browser version, the pages of my Site that you visit, the time and date of your visit, the time spent on those pages and other statistics. 
            </p>
            <hr>
            <h5 class="blog-post-title">Cookies</h5>
            <p>Cookies are files with small amount of data, which may include an anonymous unique identifier. Cookies are sent to your browser from a web site and stored on your computer’s hard drive.
                Like many sites, we use “cookies” to collect information. You can instruct your browser to refuse all cookies or to indicate when a cookie is being sent. However, if you do not accept cookies, you may not be able to use some portions of our Site.</p>
            <hr>
            <h5 class="blog-post-title">Third Parties</h5>
            <p>If you click on any of the links within our blog and are redirected to a third party site, the third party sites are not covered by this Privacy Notice.
We do not currently run advertisements of any kind.</p>
            <hr>
            <h5 class="blog-post-title">Security</h5>
            <p>The security of your Personal Information is important to us, but remember that no method of transmission over the Internet, or method of electronic storage, is 100% secure. Any breaches of data will be informed within 48 hours via a notice on this website.</p>
            <hr>
            <h5 class="blog-post-title">Changes to This Privacy Policy</h5>
            
            <p>This Privacy Policy is effective as of 5th March 2019 and will remain in effect except with respect to any changes in its provisions in the future, which will be in effect immediately after being posted on this page.
We reserve the right to update or change our Privacy Policy at any time and you should check this Privacy Policy periodically. Your continued use of the Service after we post any modifications to the Privacy Policy on this page, will constitute your acknowledgement of the modifications and your consent to abide and be bound by the modified Privacy Policy.</p>
            </div>
           </div>
        
        <!-- FOOTER with links to Privacy Policy and Contact Form -->
    <footer class="container">
        <p class="float-right"><a href="#">Back to top</a></p>
        <p>&copy; Codettes 2019 &middot; <a href="../index.php">Home</a>&middot; <a href="contactUs.php">Contact Us</a></p>
    </footer>
        
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    </body>
</html>