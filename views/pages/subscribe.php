 <?php

//include 'Subscibeconnection.php';
include 'subscription.php';
include 'DatabaseConnection.php';


 //session_start();  
 ?>
<!DOCTYPE html>
<html lang="en">
<head>
  <!-- Theme Made By www.w3schools.com - No Copyright -->
  <title>Log In</title>
  <meta charset="utf-8">    
  <meta name="viewport" content="width=device-width, initial-scale=1">
     <link rel="icon" href="../images/authorimages/women_empowered.jpg">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
  
  <style>
  body {
    font: 20px Montserrat, sans-serif;
    line-height: 1.8;
    color: #f5f6f7;
  }
  p {font-size: 16px;}
  .margin {margin-bottom: 1px;}
  .bg-1 { 
    background-color: #f76fde ; /* pink */
    color: #000000; /*black*/
  }
  .bg-2 { 
    background-color: #ffffff; /* pink */
    color: #000000;
  }
  .bg-3 { 
       background-color: #ffffff; /* White */
    color: #000000; 
    font-family: sans-serif;
    font-size: large;
  }
  .bg-4 { 
    background-color: #2f2f2f; /* Black Gray */
    color: #fff;
  }
  .container-fluid {
    padding-top: 20px;
    padding-bottom: 20px;
  }
  .container {
      padding-right:10px;
      padding-left: 10px;
  }
  .navbar {
    padding-top: 15px;
    padding-bottom: 15px;
    border: 0;
    border-radius: 0;
    margin-bottom: 0;
    font-size: 12px;
    letter-spacing: 5px;
  }
  .navbar-nav  li a:hover {
    color: #1abc9c !important;
  }
  .container form {
       background-color: #ffffff; /* White */
    color: #000000; 
    font-family: sans-serif;
    font-size: large;
    
  }
  </style>
  </head>
<body>

<!-- Navbar -->
<nav class="navbar navbar-default">
        <div class="nav-scroller pb-1 mb-2">
        <nav class="nav d-flex justify-content-between">
          <a href="/FinalProject_Blog/index.php" class="btn btn-default btn-lg">
    <span class="glyphicon glyphicon"></span> Home
  </a>

</nav>

<!-- First Container -->
<div class="container-fluid bg-1 text-center">
  <img src="../images/themes/bcreativeb_carousel.png" class="img-responsive">

 <!-- Second Container-->
  <div class="container bg-2 text-center">
        <h2>Sign up to our newsletter</h2>
        <p>Please fill in your details to subscribe to our newsletter</p>
    </div>
 <!--Third Container -->
 <div class="container form-inline">
 
 
</div>
 <div>
     <br>
     <br>

 </div>
           <?php
    echo "<form method='POST' action='".setSubscribe($dbconn)."'>
               <input type='text' name='firstname' placeholder='firstname'>
               <input type='text' name='lastname' placeholder='lastname'>
                   <input type='text' name='email' placeholder='email'>
                   <button type='submit' name='Signup'>Sign me up</button>
           </form>";
    
           ?>
 


<!-- Footer -->
<br>
<br>

        <footer class="container-fluid bg-4 text-center">
        <p>&copy; Codettes 2019 &middot; <a href="priv.php">Privacy Policy</a>&middot; <a href="newForm.php">Contact Us</a></p>
    </footer>

            
   </body>
</html>